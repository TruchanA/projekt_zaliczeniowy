from django.db import models


class Voivodeship(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class City(models.Model):
    name = models.CharField(max_length=50)
    voivodeship = models.ForeignKey(Voivodeship, on_delete=models.CASCADE, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cities'

class Person(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    birthdate = models.CharField(max_length=50,null=True,blank=True)
    voivodeship = models.ForeignKey(Voivodeship,on_delete=models.SET_NULL,null=True,blank=True)
    city = models.ForeignKey(City,on_delete=models.SET_NULL,null=True,blank=True)
    pesel = models.CharField(max_length=11,null=True,blank=True)

class Comment(models.Model):
    city = models.ForeignKey(City,on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return