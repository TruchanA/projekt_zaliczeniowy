from django import forms

from .models import Person, City


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['city'].queryset = City.objects.none()

        if 'voivodeship' in self.data:
            try:
                voivodeship_id = int(self.data.get('voivodeship'))
                self.fields['city'].queryset = City.objects.filter(voivodeship_id=voivodeship_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['city'].queryset = self.instance.voivodeship.city_set.order_by('name')