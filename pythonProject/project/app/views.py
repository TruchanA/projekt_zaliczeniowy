from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate,login,logout

from django.contrib import messages

from .forms import PersonForm
from .models import Person
from .models import City

def home_view(request):
    return render(request, "welcome.html")

def person_add_view(request):
    my_form = PersonForm()
    cities = City.objects.all
    if request.method=="POST":
        my_form = PersonForm(request.POST)
        if my_form.is_valid():
            Person.objects.create(**my_form.cleaned_data)
            return redirect('persons_view')
    context = {
        "form": my_form
    }
    return render (request, "person_add.html", context)

def persons_view(request):
    model = Person
    persons = Person.objects.all()
    context = {
        'object_list': persons,
    }
    return render(request, "app/person_list.html",context)

def person_update_view(request, pk):
    person = get_object_or_404(Person, pk=pk)
    form = PersonForm(instance=person)
    if request.method == 'POST':
        form = PersonForm(request.POST, instance=person)
        if form.is_valid():
            form.save()
            return redirect('persons_view')
    return render(request, 'person_add.html', {'form': form})


def load_cities(request):
    voivodeship_id = request.GET.get('voivodeship_id')
    cities = City.objects.filter(voivodeship_id=voivodeship_id).all()
    return render(request, 'city_dropdown_list.html', {'cities': cities})

def login_page(request):
     login_page = True
     if request.user.is_authenticated:
        return redirect('index')
     else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('index')
            else:
                messages.info(request, 'Username OR password is incorrect')
        context = {
            'login_page': login_page
        }
        return render(request, 'login.html', context)

def logout_f (request):
    logout(request)
    return render(request, "welcome.html")