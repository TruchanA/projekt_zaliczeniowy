from django.contrib import admin
from .models import Person,City,Voivodeship, Comment

class CommentsInLine(admin.TabularInline):
    model = Comment

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ['id','nameAndSurname']
    list_filter = ['city']
    search_fields = ['surname']

    def nameAndSurname(self,obj):
        return obj.name + ' ' + obj.surname

    nameAndSurname.short_description = 'Imię i nazwisko'

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ['id','name']
    search_fields = ['name']
    inlines = [
        CommentsInLine
    ]

@admin.register(Voivodeship)
class VoivodeshipAdmin(admin.ModelAdmin):
    search_fields = ['name']

@admin.register(Comment)
class Comment(admin.ModelAdmin):
    pass